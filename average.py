#Author: Raghav Bakshi
import gdal
import sys
import getopt
import argparse
import numpy

def calc(filename, band_num, method):

    driver = gdal.GetDriverByName("GTiff")
    driver.Register()

    data = gdal.Open(filename)
    rows = data.RasterYSize
    columns = data.RasterXSize
    band = data.GetRasterBand(band_num)
    band_cells=band.ReadAsArray(0, 0, columns, rows).asType(numpy.float)
    num_non_zero= 0
    total= 0
    denom= rows*columns

    for x in band_cells:
        for y in x:
            total+= y

            if y!= 0:
                num_non_zero+=1

    if method == 1:
        return float(total/denom)

    elif method == 2:
        return float(total/num_non_zero)

    else:
        return "Not a valid method"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('file_', help = "Which file do you want to read?")
    parser.add_argument('band_num', help = "Select the band the number")
    parser.add_argument('method', help = "Enter 1 if you want to include 0 values, enter 2 if you don't")
    args = parser.parse_args()
       
    print calc(args.file_, int(args.band_num), int(args.method))
   

if __name__ == "__main__":
    main()